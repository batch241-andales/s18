
/*
	function functionName(parameter){
		code block
	}

	functionName(argument)
*/

//name is called parameter
//"parameter" acts as named variable/container that exists only inside of a function
//it is used to store information that is provided to a function when it is called/invoked
let name = "John";
function printName(name){
	console.log("My name is " + name);
}


//"Juana" and "Eric", the information/data provided directly into the function, it is called an argument
printName("Juana");
printName("Eric");

let userName = "Elaine";

printName(userName);
printName(name);
printName();

function greeting() {
	console.log("Hello, User!");
}

greeting("Eric");


//Using Multiple Parameters
//
function createFullName(firstName, middleName, lastName){
	console.log(firstName + " " + middleName + " " + lastName);
}

createFullName("Erven", "Joshua", "Cabral");

//"Erven" will be stored in the parameter "firstName"
//"Joshua" will be stored in the parameter "middleName"
//"Cabral" will be stored in the parameter "lastName"

//In JS, providing more/less arguments than the expected parameters will no return an error
createFullName("Eric", "Andales");
createFullName("Roland", "John", "Doroteo", "Jr");

function checkDivisiblityBy8(num){
	let remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is " + remainder);
	let checkDivisiblityBy8 = (remainder === 0)
	console.log("Is " + num + " divisible by 8?");
	console.log(checkDivisiblityBy8);
}

checkDivisiblityBy8(64);
checkDivisiblityBy8(28);

/*
	Mini-Activity:
		1. Create a function which is able to receive data as an argument.
			-This function should be able to receive the name of your favorite superhero.
			-Display the name of your favorite superhero

*/


function displayFavoriteHero (hero) {
	console.log ("My favorite hero: " + hero);
}

displayFavoriteHero("Batman");


//Return Statement

/*
	The "return" statement allows us to ouptu a value from a function to be passed to the line/block of code that invoked/called the funtion

*/

function returnFullName (firstName, middleName, lastName){

	return firstName + " " + middleName + " " + lastName;
	console.log("Can we print this message")
}

//Whatever value that is returned from the "returnFullName" function can be stored in a variable

let completeName = returnFullName("Eric","Basarte","Andales");
console.log(completeName);


function returnAddress(city, country){
	let fullAddress = city + ", " + country;
	return fullAddress;
}

let myAddress = returnAddress("Iligan","PH");
console.log(myAddress);

//Consider the ff code.

/*
	Mini-Activity
*/
function printPlayerInformation(username, level, job){
/*	console.log("Username " + username);
	console.log("Level " + level);
	console.log("Job " + job);*/
	return "Username: " + username + "\n" + "Level: " + level + "\n" + "Job: " + job;
}



let user1 = printPlayerInformation("cardo123", "999", "Immortal")
console.log(user1);

//Returns undefined because printPlayerInformation returns nothing. It only logs the message in the console.
//You cannot save any value from printPlayerInfo() because it does not RETURN anything


