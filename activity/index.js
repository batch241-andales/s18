function addNumbers(num1, num2){
	console.log("Displayed sum of " + num1 + " and " + num2 + ": ");
	console.log((num1 + num2));

}
addNumbers(5,15);

function subtractNumbers(num1, num2){
	console.log("Displayed difference of " + num1 + " and " + num2 + ": ");
	console.log((num1 - num2));
}
subtractNumbers(20,5);

function multiplyNumbers(num1, num2){
	console.log("The product of " + num1 + " and " + num2 + ":")
	return num1 * num2;
}

let product = multiplyNumbers(50,10);
console.log(product);

function dividedNumbers(num1, num2){
	console.log("The quotient of " + num1 + " and " + num2 + ":")
	return num1 / num2;
}

let quotient = dividedNumbers(50,10);
console.log(quotient);

function areaCircle(radius){
	console.log("The result of getting the area of a circle with " + radius + " radius:")
	area = 3.1416 * (radius ** 2);
	return area;
}

let circleArea = areaCircle(15);
console.log(circleArea);

function averageNumbers(num1, num2, num3, num4){
	console.log("The average of " + num1 + " " + num2 + " " + num3 + " and " + num4 + ":")
	return (num1+num2+num3+num4)/4
}

let averageVar = averageNumbers(20,40,60,80);
console.log(averageVar);

function isPass(score,total){
	console.log("Is " + score +"/" + total + " a passing score?")
	let percentage = (score / total) * 100;
	return percentage >= 75;
}

let isPassingScore = isPass(38,50);
console.log(isPassingScore);